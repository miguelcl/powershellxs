$Server = Read-Host -Prompt 'Ingrese '
$User = Read-Host -Prompt 'Input the user name'
$Date = Get-Date
Write-Host "You input server '$Servers' and '$User' on '$Date'"


# *******************************************************************
# ActiveXperts Network Component Sample - DNS Query
# Written by ActiveXperts Software - https://www.activexperts.com
# ********************************************************************

# ***************************************************************************
# Function ReadInput
# ***************************************************************************
Function ReadInput($strPrompt, $strDefaultValue, $bAllowEmpty)
{ 
  $strReturn = ""  
  If ($strDefaultValue -ne "")
  {
     $strPrompt += " (leave empty for " + $strDefaultValue + "): "
  }
  Do 
  {       
    Write-Host $strPrompt
    $strReturn = read-host
    
    If ($strReturn -eq "" -and $strDefaultValue -ne "")
    {
      $strReturn = $strDefaultValue
      Write-Host $strReturn
    }
    elseif ($strReturn -eq "" -and $bAllowEmpty -eq $True)
    {
      break
    }   
  } While ($strReturn -eq "") 
  Write-Host ""
  return $strReturn
}


# ***************************************************************************
# MAIN SCRIPT
# ***************************************************************************

cls

# Create a socket instance
$objDnsServer = new-object -comobject AxNetwork.DnsServer
$objConstants = new-object -comobject AxNetwork.NwConstants

# A license key is required to unlock this component after the trial period has expired.
# Call 'Activate' with a valid license key as its first parameter. Second parameter determines whether to save the license key permanently 
# to the registry (True, so you need to call Activate only once), or not to store the key permanently (False, so you need to call Activate
# every time the component is created). For details, see manual, chapter "Product Activation".
#
# $objDnsServer.LicenseKey = "XXXXX-XXXXX-XXXXX"  

# Display ActiveXperts Network Component Version
Write-Host "ActiveXperts Network Component " $objDnsServer.Version "`nBuild: " $objDnsServer.Build "`nModule: "  $objDnsServer.Module "`nLicense Status: " $objDnsServer.LicenseStatus "`nLicense Key: " $objDnsServer.LicenseKey "`n`n";

# Logfile
$objDnsServer.Logfile = $env:temp + "\DnsServer.log"
Write-Host "Log file used: " $objDnsServer.Logfile "`n"

# Lookup
$objDnsServer.Server  = ReadInput "Enter a DNS server" "ns1.interstroom.nl" $False 
$strHost              = ReadInput "Enter a hostname to lookup" "www.snmptools.net" $False 
$objDnsServer.Lookup($strHost, $objConstants.nwDNS_TYPE_ANY)
Write-Host "Lookup, result: " $objDnsServer.LastError " (" $objDnsServer.GetErrorDescription( $objDnsServer.LastError ) ")"
if($objDnsServer.LastError -ne 0)
{
  exit
}


If($objDnsServer.IsAuthoritative -ne $False )
{
  Write-Host "Server is an authority for this domain"
}
Else
{
  Write-Host "Server is not an authority for this domain"
}

Write-Host " "
$objDnsRecord = $objDnsServer.GetFirstRecord()
While($objDnsServer.LastError -eq 0)
{ 
  switch($objDnsRecord.Type)
  {
    $objConstants.nwDNS_TYPE_A
    { 
      Write-Host "Type             : A"
      Write-Host "Name             : " $objDnsRecord.Name
      Write-Host "IPv4 Address     : " $objDnsRecord.Address
    }

    $objConstants.nwDNS_TYPE_AAAA
    { 
      Write-Host "Type             : AAAA"
      Write-Host "Name             : " $objDnsRecord.Name
      Write-Host "IPv4 Address     : " $objDnsRecord.Address
    }

    $objConstants.nwDNS_TYPE_CNAME
    { 
      Write-Host "Type             : CNAME"
      Write-Host "Name             : " $objDnsRecord.Name
      Write-Host "Alias            : " $objDnsRecord.Address
    }
    $objConstants.nwDNS_TYPE_MX
    { 
      Write-Host "Type             : MX"
      Write-Host "Name             : " $objDnsRecord.Name
      Write-Host "Preference       : " $objDnsRecord.Address
      Write-Host "Mail Exchange    : " $objDnsRecord.MailExchange
    }
    $objConstants.nwDNS_TYPE_NS
    { 
      Write-Host "Type             : NS"
      Write-Host "Name             : " $objDnsRecord.Name
      Write-Host "Name Server      : " $objDnsRecord.NameServer 
    }

    $objConstants.nwDNS_TYPE_PTR
    { 
      Write-Host "Type             : NS"
      Write-Host "Name             : " $objDnsRecord.Name
      Write-Host "Host             : " $objDnsRecord.Address
    }
    $objConstants.nwDNS_TYPE_SOA
    { 
      Write-Host "Type             : SOA"
      Write-Host "Name             : " $objDnsRecord.Name
      Write-Host "Name Server      : " $objDnsRecord.NameServer
      Write-Host "MailBox          : " $objDnsRecord.MailBox
      Write-Host "Serial           : " $objDnsRecord.SerialNumber
      Write-Host "Refresh          : " $objDnsRecord.RefreshInterval
      Write-Host "Retry Interval   : " $objDnsRecord.RetryInterval
      Write-Host "Expiration Limit : " $objDnsRecord.ExpirationLimit
      Write-Host "Minimum TTL      : " $objDnsRecord.MinimumTTL
    }
  }

  Write-Host " "
  $objDnsRecord =  $objDnsServer.GetNextRecord()
}

Write-Host "Finished."

###
Set-ExecutionPolicy -unrestricted